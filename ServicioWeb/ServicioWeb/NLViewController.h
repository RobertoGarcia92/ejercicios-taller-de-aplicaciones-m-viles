//
//  NLViewController.h
//  ServicioWeb
//
//  Created by Eric Roberto García Félix on 25/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController <NSXMLParserDelegate>
@property (strong, nonatomic) IBOutlet UITextField *texto;
- (IBAction)terminarEscribir:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *etiqueta;
@property (strong, nonatomic) NSMutableData *datosWeb;


@end
