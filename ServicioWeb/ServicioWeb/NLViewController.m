//
//  NLViewController.m
//  ServicioWeb
//
//  Created by Eric Roberto García Félix on 25/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import "NLViewController.h"
NSMutableString *contenidoNodo;
NSString *datosFinales;

@interface NLViewController ()

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)terminarEscribir:(id)sender {
    contenidoNodo = [[NSMutableString alloc]init];
    [self.texto resignFirstResponder];
    NSString *formatoSoap = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding =\"utf-8\"?>\n" "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap = \"http://schemas.xmlsoap.org/soap/envelope/\">\n" "<soap:Body>\n" "<CelsiusToFahrenheit xmlns=\"http://tempuri.org/\">\n" "<Celsius>%@<Celsius>\n" "</CelsiusToFahrenheit>\n" "</soap:Body>\n" "</soap:Envelope>\n", self.texto.text];
    NSURL *direccionServicioWeb = [NSURL URLWithString:@"http://www.w3schools.com/webservices/tempconvert.asmx"];
    NSMutableURLRequest *peticion = [[NSMutableURLRequest alloc]initWithURL:direccionServicioWeb];
    NSString *longitudMensaje = [NSString stringWithFormat:@"%d",[formatoSoap length]];
    [peticion addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    [peticion addValue:@"http://tempuri.org/CelsiusToFahrenheit" forHTTPHeaderField:@"SOAPAction"];
    [peticion addValue:longitudMensaje forHTTPHeaderField:@"Content-Length"];
    [peticion setHTTPMethod:@"POST"];
    [peticion setHTTPBody:[formatoSoap dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLConnection *conexion = [[NSURLConnection alloc]initWithRequest:peticion delegate:self];
    if(conexion){
        _datosWeb = [[NSMutableData alloc]init];
    }
    else{
        NSLog(@"No se estableció conexión");
    }
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [_datosWeb setLength:0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_datosWeb appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"error en la conexión");
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSXMLParser *xmlParser = [[NSXMLParser alloc]initWithData:_datosWeb];
    //NSString *elXML = [[NSString alloc]initWithBytes:[_datosWeb mutableBytes] length:[_datosWeb length] encoding:NSUTF8StringEncoding];
    [xmlParser setDelegate:self];
    [xmlParser parse];
}
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
}
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    [contenidoNodo appendString:[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
}
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    if([elementName isEqualToString:@"CelsiusToFahrenheitResult"]){
        datosFinales = contenidoNodo;
        self.etiqueta.text = datosFinales;
    }
    self.etiqueta.text = datosFinales;
}

@end
