//
//  CCAppDelegate.h
//  ControlesComunes
//
//  Created by Eric Roberto García Félix on 04/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
