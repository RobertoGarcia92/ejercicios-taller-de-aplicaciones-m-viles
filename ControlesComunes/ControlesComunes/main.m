//
//  main.m
//  ControlesComunes
//
//  Created by Eric Roberto García Félix on 04/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCAppDelegate class]));
    }
}
