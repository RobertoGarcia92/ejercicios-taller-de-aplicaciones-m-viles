//
//  NLAppDelegate.h
//  ServicioJSON
//
//  Created by Eric Roberto García Félix on 25/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
