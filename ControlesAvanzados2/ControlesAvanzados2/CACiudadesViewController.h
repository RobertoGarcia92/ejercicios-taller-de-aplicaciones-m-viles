//
//  CACiudadesViewController.h
//  ControlesAvanzados2
//
//  Created by Eric Roberto García Félix on 06/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CACiudadesViewController : UITableViewController

@property NSArray *ciudades;

@end
