//
//  main.m
//  ControlesAvanzados2
//
//  Created by Raul Alberto Vega Ruelas on 17/02/14.
//  Copyright (c) 2014 Raul Alberto Vega Ruelas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CAAppDelegate class]));
    }
}
