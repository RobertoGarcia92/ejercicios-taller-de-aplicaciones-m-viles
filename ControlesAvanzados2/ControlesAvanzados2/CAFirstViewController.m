//
//  CAFirstViewController.m
//  ControlesAvanzados2
//
//  Created by Eric Roberto García Félix on 06/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import "CAFirstViewController.h"

@interface CAFirstViewController ()

@end

@implementation CAFirstViewController

NSArray *unidades;
int tipoDeUnidades=0;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    unidades = [NSArray arrayWithObjects:[NSArray arrayWithObjects:@"Longitud",@"centimetros",@"metros",@"pies", nil], [NSArray arrayWithObjects:@"Area",@"hectareas",@"metros cuadrados", nil],[NSArray arrayWithObjects:@"Volumen",@"litros",@"galones",@"metros cubicos", nil],nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark pickerView

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2; //2 columnas en el pickerView
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return [unidades count]; //regresa cuantos tipos de unidades tenemos
    }
    return [[unidades objectAtIndex:tipoDeUnidades] count]-1;
    //regresamos cuantas unidades de este tipo tenemos restando el titulo
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) { //si cambia de seleccion el tipo
        if (tipoDeUnidades != row) {
            tipoDeUnidades=row; //guardamos el nuevo tipo
            [pickerView reloadComponent:1]; //mandamos recargar las unidades del tipo
        }
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return [[unidades objectAtIndex:row]objectAtIndex:0];
    }
    return [[unidades objectAtIndex:tipoDeUnidades]objectAtIndex:row+1];
}

@end
