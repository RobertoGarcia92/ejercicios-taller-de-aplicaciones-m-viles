//
//  NLViewController.m
//  VistaWeb2
//
//  Created by Eric Roberto García Félix on 19/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *navegador;

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *direccion = @"http://www.google.com";
    NSURL *url = [NSURL URLWithString:direccion];
    NSURLRequest *peticion = [NSURLRequest requestWithURL:url];
    [self.navegador loadRequest:peticion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
