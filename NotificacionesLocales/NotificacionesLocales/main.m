//
//  main.m
//  NotificacionesLocales
//
//  Created by pablo borquez on 01/03/14.
//  Copyright (c) 2014 pableiros. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NLAppDelegate class]));
    }
}
