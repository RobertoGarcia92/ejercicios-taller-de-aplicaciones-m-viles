//
//  NLViewController.h
//  NotificacionesLocales
//
//  Created by Eric Roberto García Félix on 08/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLViewController : UIViewController

- (IBAction)notificar:(id)sender;

@end
