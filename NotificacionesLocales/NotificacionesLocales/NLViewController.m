//
//  NLViewController.m
//  NotificacionesLocales
//
//  Created by Eric Roberto García Félix on 08/03/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)notificar:(id)sender {
    NSDate *alertTime = [[NSDate date]dateByAddingTimeInterval:10];         //creamos fecha a partir de 10 segundos de la fecha actual.
    UIApplication *app = [UIApplication sharedApplication];
    UILocalNotification *notifyAlarm = [[UILocalNotification alloc]init];   //creamos notificacion
    
    if (notifyAlarm) {                                                      //si se creó correctamente la notificación
        notifyAlarm.fireDate = alertTime;                                   //indicamos fecha y hora que se lanzará la notificación
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;                                     //cuantas veces se debe de repetir
        [notifyAlarm setApplicationIconBadgeNumber:1];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        notifyAlarm.soundName = @"Glass.aiff";
        notifyAlarm.alertBody = @"Esta es una notificación";
        [app scheduleLocalNotification:notifyAlarm];                        //la agregamos a la calendarización de notificaciones
    }
}
@end
