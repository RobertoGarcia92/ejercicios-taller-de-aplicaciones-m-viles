//
//  XYZAppDelegate.h
//  Calculadora
//
//  Created by Eric Roberto García Félix on 17/02/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
