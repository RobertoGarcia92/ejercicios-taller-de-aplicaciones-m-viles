//
//  XYZViewController.m
//  Calculadora
//
//  Created by Eric Roberto García Félix on 17/02/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import "XYZViewController.h"

@interface XYZViewController ()

@end

@implementation XYZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}


-(IBAction)button1{
    if([display.text isEqual:@"0"]){
        display.text = @"1";
    }else{
        display.text = [NSString stringWithFormat:@"%@1", display.text];
    }
}
-(IBAction)button2{
    if([display.text isEqual:@"0"]){
        display.text = @"2";
    }else{
        display.text = [NSString stringWithFormat:@"%@2", display.text];
    }
}
-(IBAction)button3{
    if([display.text isEqual:@"0"]){
        display.text = @"3";
    }else{
        display.text = [NSString stringWithFormat:@"%@3", display.text];
    }
}
-(IBAction)button4{
    if([display.text isEqual:@"0"]){
        display.text = @"4";
    }else{
        display.text = [NSString stringWithFormat:@"%@4", display.text];
    }
}
-(IBAction)button5{
    if([display.text isEqual:@"0"]){
        display.text = @"5";
    }else{
        display.text = [NSString stringWithFormat:@"%@5", display.text];
    }
}
-(IBAction)button6{
    if([display.text isEqual:@"0"]){
        display.text = @"6";
    }else{
        display.text = [NSString stringWithFormat:@"%@6", display.text];
    }
}
-(IBAction)button7{
    if([display.text isEqual:@"0"]){
        display.text = @"7";
    }else{
        display.text = [NSString stringWithFormat:@"%@7", display.text];
    }
}
-(IBAction)button8{
    if([display.text isEqual:@"0"]){
        display.text = @"8";
    }else{
        display.text = [NSString stringWithFormat:@"%@8", display.text];
    }
}
-(IBAction)button9{
    if([display.text isEqual:@"0"]){
        display.text = @"9";
    }else{
        display.text = [NSString stringWithFormat:@"%@9", display.text];
    }
}
-(IBAction)button0{
    if(![display.text isEqual:@"0"]){
        display.text = [NSString stringWithFormat:@"%@0", display.text];
    }
}
-(IBAction)buttonDot{
    display.text = [NSString stringWithFormat:@"%@.", display.text];
}
-(IBAction)masButton{
    operation = PLUS;
    storage = display.text;
    display.text = @"";
    displayoperation.text=@"+";
}
-(IBAction)menosButton{
    operation = MINUS;
    storage = display.text;
    display.text = @"";
    displayoperation.text=@"-";
}
-(IBAction)multiplicaButton{
    operation = MULTIPLY;
    storage = display.text;
    display.text = @"";
    displayoperation.text=@"*";
}
-(IBAction)divideButton{
    operation = DIVIDE;
    storage = display.text;
    display.text = @"";
    displayoperation.text=@"/";
}
-(IBAction)buttonEquals{
    NSString *secondNumber = display.text;
    Boolean check  = true;
    switch (operation) {
        case PLUS:
            storagenum = [NSString stringWithFormat:@"%f", [storage doubleValue] + [secondNumber doubleValue]];
            while ([storagenum hasSuffix:@"0"] && [storagenum length]>1 && check == true){
                storagenum = [storagenum substringToIndex:[storagenum length]-1];
            }
            while([storagenum hasSuffix:@"."]){
                storagenum = [storagenum substringToIndex:[storagenum length]-1];
                check=false;
            }
            display.text = storagenum;
            check=true;
            break;
        case MINUS:
            storagenum = [NSString stringWithFormat:@"%f", [storage doubleValue] - [secondNumber doubleValue]];
            while ([storagenum hasSuffix:@"0"] && [storagenum length]>1 && check == true){
                storagenum = [storagenum substringToIndex:[storagenum length]-1];
            }
            while([storagenum hasSuffix:@"."]){
                storagenum = [storagenum substringToIndex:[storagenum length]-1];
                check=false;
            }
            display.text = storagenum;
            check=true;
            break;
        case MULTIPLY:
            storagenum = [NSString stringWithFormat:@"%f", [storage doubleValue] * [secondNumber doubleValue]];
            while ([storagenum hasSuffix:@"0"] && [storagenum length]>1 && check == true){
                storagenum = [storagenum substringToIndex:[storagenum length]-1];
            }
            while([storagenum hasSuffix:@"."]){
                storagenum = [storagenum substringToIndex:[storagenum length]-1];
                check=false;
            }
            display.text = storagenum;
            check=true;
            break;
        case DIVIDE:
            storagenum = [NSString stringWithFormat:@"%f", [storage doubleValue] / [secondNumber doubleValue]];
            while ([storagenum hasSuffix:@"0"] && [storagenum length]>1 && check == true){
                storagenum = [storagenum substringToIndex:[storagenum length]-1];
            }
            while([storagenum hasSuffix:@"."]){
                storagenum = [storagenum substringToIndex:[storagenum length]-1];
                check=false;
            }
            display.text = storagenum;
            check=true;
            break;
    }
    displayoperation.text = @"";
}
-(IBAction)buttonDel{
    if([display.text length] > 1){
        display.text = [display.text substringToIndex:[display.text length]-1];
    }else if([display.text length] == 1){
        display.text = @"0";
    }
    else{
        //Nada para borrar.
    }
}
-(IBAction)buttonClear{
    display.text = @"0";
}
-(IBAction)buttonmasmenos{
    if(![display.text isEqual:@"0"]&&![display.text hasPrefix:@"-"]){
        display.text = 	[@"-" stringByAppendingString:display.text];
    }else if (![display.text isEqual:@"0"]&&[display.text hasPrefix:@"-"]){
        display.text = 	[display.text substringFromIndex:1];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
