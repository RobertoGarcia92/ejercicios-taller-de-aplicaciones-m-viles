//
//  XYZViewController.h
//  Calculadora
//
//  Created by Eric Roberto García Félix on 17/02/14.
//  Copyright (c) 2014 Robert Garcia. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum {PLUS, MINUS, MULTIPLY, DIVIDE} CalculatorOperation;
@interface XYZViewController : UIViewController{
    IBOutlet UILabel *display;
    IBOutlet UILabel *displayoperation;
    NSString *storage;
    NSString *storagenum;
    CalculatorOperation operation;
}
-(IBAction)button1;
-(IBAction)button2;
-(IBAction)button3;
-(IBAction)button4;
-(IBAction)button5;
-(IBAction)button6;
-(IBAction)button7;
-(IBAction)button8;
-(IBAction)button9;
-(IBAction)button0;
-(IBAction)buttonmasmenos;
-(IBAction)masButton;
-(IBAction)menosButton;
-(IBAction)multiplicaButton;
-(IBAction)divideButton;
-(IBAction)buttonEquals;
-(IBAction)buttonDel;
-(IBAction)buttonClear;
-(IBAction)buttonDot;

@end